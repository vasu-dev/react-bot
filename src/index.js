import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import ChatBot from 'react-simple-chatbot';
import { Select } from 'antd';


class CarInsuranceBot extends Component {
  render() {
    return (
      <ChatBot
        steps={[{
        id: '1',
        message: 'Hi! I\'m Claudia. Let\'s make sure you are not over paying for insurance. This will only take a minute.',
        trigger: '2',
      },
      {
        id: '2',
        message: 'To start I will need your zip code where your car is parked?',
        trigger: '3',
      },
      {
        id: '3',
        user: true,
         validator: (value) => {
        if (isNaN(value)) {
          return 'value should be a number';
        }
        return true;
      },
        trigger: '4',
      },
      {
        id: '4',
        message: 'What year is the car you drive?',
        trigger: '5',
      },
      {
        id: '5',
        options: [
          { value: 1, label: '2010-2018', trigger: '6' },
          { value: 2, label: '2000-2009', trigger: '7' },
          { value: 3, label: '1990-1999', trigger: '8' },
          { value: 4, label: '1980-1989', trigger: '9' },
          { value: 4, label: 'pre-1980', trigger: '10' },
        ],
      },
      {
        id: '6',
        options: [
          { value: 2018, label: '2018', trigger: '10' },
          { value: 2017, label: '2017', trigger: '10' },
          { value: 2016, label: '2016', trigger: '10' },
          { value: 2015, label: '2015', trigger: '10' },
          { value: 2014, label: '2014', trigger: '10' },
          { value: 2013, label: '2013', trigger: '10' },
          { value: 2012, label: '2012', trigger: '10' },
          { value: 2011, label: '2011', trigger: '10' },
          { value: 2010, label: '2010', trigger: '10' },
        ],
      },
      {
        id: '7',
        options: [
          { value: 2009, label: '2009', trigger: '10' },
          { value: 2008, label: '2008', trigger: '10' },
          { value: 2007, label: '2007', trigger: '10' },
          { value: 2006, label: '2006', trigger: '10' },
          { value: 2005, label: '2005', trigger: '10' },
          { value: 2004, label: '2004', trigger: '10' },
          { value: 2003, label: '2003', trigger: '10' },
          { value: 2002, label: '2002', trigger: '10' },
          { value: 2001, label: '2001', trigger: '10' },
          { value: 2000, label: '2000', trigger: '10' },
        ],
      },
      {
        id: '8',
        options: [
          { value: 1999, label: '1999', trigger: '10' },
          { value: 1998, label: '1998', trigger: '10' },
          { value: 1997, label: '1997', trigger: '10' },
          { value: 1996, label: '1996', trigger: '10' },
          { value: 1995, label: '1995', trigger: '10' },
          { value: 1994, label: '1994', trigger: '10' },
          { value: 1993, label: '1993', trigger: '10' },
          { value: 1992, label: '1992', trigger: '10' },
          { value: 1991, label: '1991', trigger: '10' },
          { value: 1990, label: '1990', trigger: '10' },
        ],
      },
      {
        id: '9',
        options: [
          { value: 1989, label: '1989', trigger: '10' },
          { value: 1988, label: '1988', trigger: '10' },
          { value: 1987, label: '1987', trigger: '10' },
          { value: 1986, label: '1986', trigger: '10' },
          { value: 1985, label: '1985', trigger: '10' },
          { value: 1984, label: '1984', trigger: '10' },
          { value: 1983, label: '1983', trigger: '10' },
          { value: 1982, label: '1982', trigger: '10' },
          { value: 1981, label: '1981', trigger: '10' },
          { value: 1980, label: '1980', trigger: '10' },
        ],
      },
      {
        id: '10',
        message: 'What is the make of your car?',
        trigger: '11',
      },
      {
        id: '11',
        options: [
          { value: 1, label: 'A-D', trigger: '12' },
          { value: 2, label: 'E-K', trigger: '13' },
          { value: 3, label: 'L-O', trigger: '14' },
          { value: 4, label: 'P-Z', trigger: '15' },
        ],
      },
      {
        id: '12',
        options: [
          { value: 'ACURA', label: 'ACURA', trigger: '16' },
          { value: 'ASTON MARTIN', label: 'ASTON MARTIN', trigger: '16' },
          { value: 'AUDI', label: 'AUDI', trigger: '16' },
          { value: 'BENTLEY', label: 'BENTLEY', trigger: '16' },
          { value: 'BMW', label: 'BMW', trigger: '16' },
          { value: 'BUICK', label: 'BUICK', trigger: '16' },
          { value: 'CARDILLAC', label: 'CARDILLAC', trigger: '16' },
          { value: 'CHEVROLET', label: 'CHEVROLET', trigger: '16' },
          { value: 'CHRYSLER', label: 'CHRYSLER', trigger: '16' },
          { value: 'DODGE', label: 'DODGE', trigger: '16' },
        ],
      },
      {
        id: '13',
        options: [
          { value: 'FERRARI', label: 'FERRARI', trigger: '16' },
          { value: 'FORD', label: 'FORD', trigger: '16' },
          { value: 'GMC', label: 'GMC', trigger: '16' },
          { value: 'HONDA', label: 'HONDA', trigger: '16' },
          { value: 'HYUNDAI', label: 'HYUNDAI', trigger: '16' },
          { value: 'INFINITI', label: 'INFINITI', trigger: '16' },
          { value: 'JAGUAR', label: 'JAGUAR', trigger: '16' },
          { value: 'JEEP', label: 'JEEP', trigger: '16' },
          { value: 'KIA', label: 'KIA', trigger: '16' },
        ],
      },
      {
        id: '14',
        options: [
          { value: 'LAMBORGHINI', label: 'LAMBORGHINI', trigger: '16' },
          { value: 'LAND ROVER', label: 'LAND ROVER', trigger: '16' },
          { value: 'LEXUS', label: 'LEXUS', trigger: '16' },
          { value: 'LINCOLN', label: 'LINCOLN', trigger: '16' },
          { value: 'MASERATI', label: 'MASERATI', trigger: '16' },
          { value: 'MAZDA', label: 'MAZDA', trigger: '16' },
          { value: 'MERCEDES-BENZ', label: 'MERCEDES-BENZ', trigger: '16' },
          { value: 'MINI', label: 'MINI', trigger: '16' },
          { value: 'MITSUBHISI', label: 'MITSUBHISI', trigger: '16' },
          { value: 'NISSAN', label: 'NISSAN', trigger: '16' },
        ],
      },
      {
        id: '15',
        options: [
          { value: 'PORSCHE', label: 'PORSCHE', trigger: '16' },
          { value: 'RAM', label: 'RAM', trigger: '16' },
          { value: 'ROLLS ROYCE', label: 'ROLLS ROYCE', trigger: '16' },
          { value: 'SMART', label: 'SMART', trigger: '16' },
          { value: 'SUBARU', label: 'SUBARU', trigger: '16' },
          { value: 'TESLA', label: 'TESLA', trigger: '16' },
          { value: 'TOYOTA', label: 'TOYOTA', trigger: '16' },
          { value: 'VOLKSWAGEN', label: 'VOLKSWAGEN', trigger: '16' },
          { value: 'VOLVO', label: 'VOLVO', trigger: '16' },
        ],
      },
      {
        id: '16',
        message: 'What is the model of your car?',
        trigger: '17',
      },
      {
        id: '17',
        user: true,
        trigger: '18',
        
      },
      {
        id: '18',
        message: 'What is the collision deductible you want?',
        trigger: '19',
      },
      {
        id: '19',
        options: [
          { value: 100, label: '$100', trigger: '20' },
          { value: 250, label: '$250', trigger: '20' },
          { value: 500, label: '$500', trigger: '20' },
          { value: 1000, label: '$1000', trigger: '20' },
          { value: 2500, label: '$2500', trigger: '20' },
        ],
      },
      {
        id: '20',
        message: 'What is the comprehensive deductible you want?',
        trigger: '21',
      },
      {
        id: '21',
        options: [
          { value: 100, label: '$100', trigger: '22' },
          { value: 250, label: '$250', trigger: '22' },
          { value: 500, label: '$500', trigger: '22' },
          { value: 1000, label: '$1000', trigger: '22' },
          { value: 2500, label: '$2500', trigger: '22' },
        ],
      },
      {
        id: '22',
        message: 'When did the primary driver get their US driving license?',
        trigger: '23',
      },
      {
        id: '23',
        user: true,
        trigger: '24',
      },
      {
        id: '24',
        message: 'How many driving incidents has the primary driver had in the last three years?',
        trigger: '25',
      },
      {
        id: '25',
        options: [
          { value: 0, label: '0', trigger: '26' },
          { value: 1, label: '1', trigger: '26' },
          { value: 2, label: '2', trigger: '26' },
          { value: 3, label: '3', trigger: '26' },
          { value: 4, label: '4', trigger: '26' },
          { value: 5, label: '5', trigger: '26' },
          { value: 6, label: 'more than 5', trigger: '26' },
        ],
      },
      {
        id: '26',
        message: 'What is the age of the primary driver?',
        trigger: '27',
      },
      {
        id: '27',
        user: true,
         validator: (value) => {
        if (isNaN(value)) {
          return 'value should be a number';
        }
        return true;
      },
        trigger: '28',
      },
      {
        id: '28',
        message: 'How many driving incidents has the primary driver had in the last three years?',
        trigger: '29',
      },
      {
        id: '29',
        options: [
          { value: 0, label: '$15000/$30000/$5000', trigger: '30' },
          { value: 1, label: '$25000/$50000/$10000', trigger: '30' },
          { value: 2, label: '$50000/$100000/$25000', trigger: '30' },
          { value: 3, label: '$100000/$300000/$50000', trigger: '30' },
          { value: 4, label: '$250000/$500000/$100000', trigger: '30' },
          
        ],
      },
      {
        id: '30',
        message: 'Thanks for all your inputs!',
        end : true,
      },

    ]}
      />
    );
  }
}

export default CarInsuranceBot;

ReactDOM.render(<div><CarInsuranceBot /></div>,document.getElementById('root'));